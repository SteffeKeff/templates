param name string
param location string = 'West Europe'
//param location string = 'Sweden Central'

resource appServicePlan 'Microsoft.Web/serverfarms@2021-01-01' = {
    name: 'asp-${name}'
    location: location
    sku: {
        name: 'F1'
        tier: 'Free'
    }
}

resource webApp 'Microsoft.Web/sites@2021-01-01' = {
    name: '${name}'
    location: location
    properties: {
        serverFarmId: appServicePlan.id
        siteConfig: {
            netFrameworkVersion: 'v9.0' // Setting the target framework to .NET 9
        }
    }
}

resource webAppConfig 'Microsoft.Web/sites/config@2021-01-01' = {
    parent: webApp
    name: 'web'
    properties: {
        netFrameworkVersion: 'v9.0' // Ensure consistency with the web app
        remoteDebuggingVersion: 'VS2022' // Optional: Update to the latest supported version
    }
}