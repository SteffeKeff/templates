param name string
param location string = 'Sweden Central'

resource appServicePlan 'Microsoft.Web/serverfarms@2021-01-01' = {
    name: 'asp-${name}'
    location: location
    sku: {
        name: 'F1'
        tier: 'Free'
    }
}

resource webApp 'Microsoft.Web/sites@2021-01-01' = {
    name: 'app-${name}'
    location: location
    dependsOn: [
        logAnalyticsWorkspace
    ]
    properties: {
        serverFarmId: appServicePlan.id
        siteConfig: {
            appSettings: [
                {
                    name: 'APPINSIGHTS_INSTRUMENTATIONKEY'
                    value: appInsights.properties.InstrumentationKey
                }
            ]
        }
    }
}

resource webAppConfig 'Microsoft.Web/sites/config@2021-01-01' = {
  parent: webApp
  name: 'web'
  properties: {
    netFrameworkVersion: 'v7.0'
    remoteDebuggingVersion: 'VS2019'
  }
}

resource appInsights 'Microsoft.Insights/components@2020-02-02' = {
  name: 'appi-${name}'
  location: location
  kind: 'web'
  properties: {
    Application_Type: 'web'
    WorkspaceResourceId: logAnalyticsWorkspace.id
  }
}

resource logAnalyticsWorkspace 'Microsoft.OperationalInsights/workspaces@2020-08-01' = {
    name: 'la-${name}'
    location: location
}