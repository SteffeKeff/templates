targetScope = 'subscription'

param name string
param location string = 'West Europe'
//param location string = 'Sweden Central'

resource rg 'Microsoft.Resources/resourceGroups@2021-04-01' = {
  name: name
  location: location
}