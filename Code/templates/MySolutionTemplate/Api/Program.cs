using Api;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddEndpointsApiExplorer().AddSwaggerGen();
builder.Services.AddSingleton<Service>();

var app = builder.Build();
app.UseSwagger().UseSwaggerUI();

app.MapGet("/Welcome", (Service service) => service.Get());

app.Run();