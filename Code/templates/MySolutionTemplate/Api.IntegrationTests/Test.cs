namespace StorefrontApi.IntegrationTests;

public class Test
{
    [TestCase("Requests/Test.http")]
    public async Task Test1(string filePath)
    {
        Console.WriteLine("Running integrationTests");

        var result = await new HttpClient().SendHttpFileGetRequestAsync(filePath);
        
        await Verify(result);
        
        Console.WriteLine("Test matched verified result");
    }
}
