namespace StorefrontApi.IntegrationTests;

public static class HttpClientExtensions
{
    private static readonly string BaseUrl = Environment.GetEnvironmentVariable("baseurl") ?? "http://localhost:1337";

    public static async Task<string> SendHttpFileGetRequestAsync(this HttpClient client, string filePath)
    {
        var httpFile = await File.ReadAllTextAsync(filePath);

        var requestParts = httpFile.Split(' ');
        var httpMethod = requestParts[0];
        var url = requestParts[1].Replace("{{BaseUrl}}", BaseUrl);
        Console.WriteLine($"Testing \"{httpMethod}\" on url: {url}");
        
        var request = new HttpRequestMessage(new HttpMethod(httpMethod), url);
        var response = await client.SendAsync(request);
        response.EnsureSuccessStatusCode();
        
        var content = await response.Content.ReadAsStringAsync();
        Console.WriteLine($"Received: {content}");

        return content;
    }
}