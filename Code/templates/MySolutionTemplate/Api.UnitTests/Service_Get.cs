using AutoFixture.NUnit3;

namespace Api.UnitTests;

[TestFixture]
public class Tests
{
    [AutoData]
    public async Task Valid(Service service)
    {
        await Verify(service.Get());
    }
}